# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::base
class profile::base {
  $facter_dirs = ['/etc/puppetlabs/facter', '/etc/puppetlabs/facter/facts.d']
  include ntp

  file { $facter_dirs: 
    ensure => directory,
  }

  create_resources('file', lookup('file', Hash, 'hash', {}))
  create_resources('firewall', lookup('firewall', Hash, 'hash', {}))
  create_resources('augeas', lookup('augeas', Hash, 'hash', {}))
}
