# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::puppetboard
class profile::puppetboard {
  
  # Configure Apache on this server
  include apache
  include apache::mod::version
  include apache::mod::wsgi

  # Configure Puppetboard
  include puppetboard

  # Access Puppetboard through pboard.example.com
  include puppetboard::apache::vhost
}
